'''
Created on 15.05.2014

@author: simon
'''
from z3 import is_true, And

from smt.api.templatemodel import ApiTemplateModel


class LabelGuardedTemplateModel(ApiTemplateModel):
    def __init__(self, model, templatefunction):
        super().__init__(model, templatefunction)

        self._define_guard_bit_states_dict()

    def _define_guard_bit_states_dict(self):
        bit_offset = self._template_function.guard_bit_offset

        # get for each guard bit the conjuncts which must be satisfied
        # by the particular state outputs such that the state
        # represents the considered guard bit
        guard_output_funcs = \
            {i: self._get_output_function_value_tuple(i)
             for i in range(2 ** len(self._template_function.label_functions))}

        # check whether state matches guard label
        def _state_matches_guard(t, i):
            expr = And([func_call(t) == func_res
                        for func_call, func_res in guard_output_funcs[i]])
            return is_true(self.model.eval(expr))

        self.guard_bit_states_dict = \
            {1 << (bit_offset + i):
             {str(t) for t in self._internal_states
              if _state_matches_guard(t, i)}
             for i in range(2 ** len(self._template_function.label_functions))}

    def _get_output_function_value_tuple(self, guard_bit):
        guard_conjuncts = \
            [(func, guard_bit & (1 << i) > 0)
             for i, func in
             enumerate(self._template_function.label_functions)]
        return guard_conjuncts
