'''
Created on 15.05.2014

@author: simon
'''
import logging

from z3 import sat

from smt.api.encoder import PyZ3Encoder
from smt.encoder_base import EncodingOptimization, SMTEncoder


class LabelGuardedPyZ3Encoder(PyZ3Encoder):
    def __init__(self, spec, architecture,
                 encoding_optimization=EncodingOptimization.NONE):
        super(LabelGuardedPyZ3Encoder, self).__init__(spec,
                                                      architecture,
                                                      encoding_optimization)

    @classmethod
    def get_encoder_type(cls):
        return SMTEncoder.LABEL_GUARD_ENCODER

    def _handle_result(self, solver, result):
        logging.info("Solver result: %s" % result)
        logging.debug("Formulas")
        logging.debug(solver)
        if result == sat:
            model = {}
            guard_bit_states_dict = {}
            for t in self.encoder_info.template_functions:
                template_model = t.get_model(solver.model())
                template_model.update_guard_bit_dict(guard_bit_states_dict)
                model[t.template_index] = template_model
            for template_model in model.values():
                template_model.init_transition_guards(guard_bit_states_dict)

            logging.debug("Model")
            logging.debug(solver.model())
            return model
        return None

    def check(self):
        s = self.encoder_info.solver

        model = self._handle_result(s, s.check())
        return (model is not None, model)

    def _init_guard_size(self):
        # TODO: consider support for auxiliary variables
        num_auxiliary_variables = (0,) * len(self.spec.templates)
        self.encoder_info.guard_slice_sizes = self.__class__.get_guard_size(
            self.spec, num_auxiliary_variables)
        self.encoder_info.guard_size = sum(self.encoder_info.guard_slice_sizes)

        logging.debug("Guard slice sizes: %s",
                      str(self.encoder_info.guard_slice_sizes))

    @classmethod
    def get_guard_size(cls, spec, num_aux_vars):
        '''
        Return a sequence describing the templates' guard sizes, depending on

        * the number of labels defined by the template
        * the number of auxiliary variables

        An observable state is identified by its label. Thus, the number of
        observable states in label-based systems is equal to the number of
        possible assignments to label variables.
        For each template, we have 2^(|label variables| + |auxiliaries|)
        possible labels.

        :param cls:
        :param spec: Specification including templates
        :param num_aux_vars:
            Sequence containing the number of auxiliary variables for
            each template.
        '''
        return [2 ** (len(template.labels) + num_aux_vars[idx])
                for idx, template in enumerate(spec.templates)]

    def _encode_template_functions(self):
        # create templates
        from smt.api.labelguarded.templatefunction import TemplateFunction
        self.encoder_info.template_functions = \
            [TemplateFunction(template, self, self.encoder_info, self.spec)
             for template in self.spec.templates]
