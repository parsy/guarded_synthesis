'''
Created on 24.05.2014

@author: simon
'''
import logging

from z3 import Function, BoolSort, If, BitVecVal, \
    BitVecSort, Const, ForAll, RotateLeft
from functools import reduce

from smt.api import templatefunction
from math import log
from smt.api.labelguarded.templatemodel import LabelGuardedTemplateModel


class TemplateFunction(templatefunction.TemplateFunction):
    def __init__(self, template, encoder, encoder_info, spec):
        self._logger = logging.getLogger(__name__)
        self.guard_bit_offset = None
        self._aux_labels = None
        self.labels = None
        super().__init__(template, encoder, encoder_info, spec)

    def _define_state_guard(self):
        """
        Defines auxiliary variables and the function label values for

        * output variables that serve as labels
        * auxiliary labels

        guard_state_bit: T -> BitVec(Sum_i(2**(|labels|+|aux_labels|)))
        Returns the bit vector corresponding to a given state's labels
        """
        # define auxiliary variables
        num_aux_vars = int(log(self._encoder_info.guard_slice_sizes[
            self._template.template_index], 2)) - len(self._template.labels)
        assert num_aux_vars >= 0

        # define label to bitvec bit
        self._aux_labels = [
            Function("aux_%s_%s" % (self.template_index, i),
                     self.state_sort, BoolSort()) for i in range(num_aux_vars)]

        self._logger.debug("Added %d auxiliaries for template %d",
                           len(self._aux_labels), self.template_index)

        function_arguments = [self.state_sort,
                              BitVecSort(self._encoder_info.guard_size)]
        self.state_guard = Function('label_values_%d' %
                                    self._template.template_index,
                                    function_arguments)

        # the offset of the first bit within the guard bit vector belonging
        # to this bit.
        offset = sum(self._encoder_info.guard_slice_sizes
                           [:self._template.template_index])
        if offset > 0:
            offset = int(log(offset, 2))
        self.guard_bit_offset = offset

        label_names = [str(sig) for sig in self._template.labels]
        self._label_functions = \
            [func for func in self.output_functions
             if str(func) in label_names] + self._aux_labels
        # ensure that each label and its negation have a unique bit
        # in the bit vector
        t_i = Const('ti', self.state_sort)

        # we build the index of a certain label (a combination of all label
        # variable's outputs) within the bit vector.
        # e.g., label value 00 corresponds to the first bit (in the reserved
        # range, 01 to the second etc.)
        # body_exprs determines this binary combination based on each variable.
        body_exprs = [If(label_func(t_i),
                         BitVecVal(1 << (offset + i),
                                   self._encoder_info.guard_size), 0)
                      for i, label_func in enumerate(self._label_functions)]

        # function_body determines the index of a label within the guard bit
        # vector.
        function_body = reduce(lambda x, y: x | y, body_exprs)

        self._encoder_info.solver.append(
            ForAll(t_i, self.state_guard(t_i) == (1 << function_body)))

    @property
    def label_functions(self):
        return self._label_functions

    # @property
    # def num_label_guard_vars(self):
    #    return 2 ** (len(self.output_aux_functions) + len(self.aux_labels))

    def get_model(self, smodel):
        return LabelGuardedTemplateModel(smodel, self)
