'''
Created on 28.02.2014

@author: simon
'''
import logging
from itertools import product
from functools import reduce

from abc import abstractmethod, ABCMeta  # pylint: disable=unused-import
from z3 import Datatype, Bool, Function, BoolSort, BitVecSort, \
    ForAll, And, IntSort, Const, Or, Exists, Implies, \
    Not, UGE, UGT, Tactic, Then, IntVal, BoolVal, unsat, BitVecVal

from helpers.rejecting_states_finder import build_state_to_rejecting_scc
from smt.api.architectureencoder import ArchitectureEncoder
from smt.encoder_base import SMTEncoder, EncodingOptimization
from smt.api.encoder_info import PyZ3EncoderInfo
from architecture import scheduler


class PyZ3Encoder(SMTEncoder, metaclass=ABCMeta):
    '''
    Encodes the bounded synthesis problem using the Python Z3 API
    '''
    def __init__(self, spec, architecture,
                 encoding_optimization=EncodingOptimization.NONE):
        super(PyZ3Encoder, self).__init__(spec, architecture,
                                          encoding_optimization)
        self.encoder_info = None
        self.__logger__ = logging.getLogger(__name__)

    @classmethod
    def get_encoder_type(cls):
        '''
        Returns the encoder type
        '''
        return None

    def encode(self):
        '''
        Adds architectural and template specific constraints to the SMT problem
        '''
        self.encoder_info = PyZ3EncoderInfo()
        self.encoder_info.solver = Then(Tactic("qe"), Tactic("smt")).solver()

        self.encoder_info.spec = self.spec
        self.encoder_info.architecture_encoder = \
                ArchitectureEncoder(self.encoder_info, self.architecture)

        self._init_guard_size()

        # define common functions
        self._define_eval_guard()

        self._encode_template_functions()

        self.encoder_info.architecture_encoder.add_guard_constraints()

    def _define_eval_guard(self):
        """Defines the function eval_guard: BitVec x BitVec -> Bool

        Function arguments:

        * s_s -- current global state set
        * s_g -- guard set
        """
        self.encoder_info.architecture_encoder.define_eval_guard()

    def _compare_scheduling(self, scheduling_signals, scheduling_assignment,
                            transition_label):
        '''
        Checks whether a given UCT transition label dictionary is such that it
        either contains no information about schedulings or matches the current
        scheduling

        :note: The order of `scheduling_signals` and `scheduling_assignment`
               must match.

        :param scheduling_signals: Scheduling signals as used by the transition
                                   label
        :param scheduling_assignment: Current scheduling assignment
        :param transition_label: Considered transition label
        '''
        for i, signal in enumerate(scheduling_signals):
            if transition_label.__contains__(signal) and \
                    transition_label[signal] != scheduling_assignment[i]:
                return False
        return True

    def _build_state_guard_expression(self, global_state_tuple_expressions,
                                      current_process):
        # convert all local states to bit vector

        guard_bit_expressions = \
            [self.encoder_info.template_functions[k].state_guard(state_expr)
             for (k, i), state_expr in global_state_tuple_expressions.items()
             if (k, i) != current_process]
            #             [self.encoder_info.template_functions[0].state_guard(
#              self.encoder_info.template_functions[0].get_initial_states()[0])] + \
        guard_bit_expressions += [BitVecVal(0, self.encoder_info.guard_size)]
        return reduce(lambda x, y: x | y, guard_bit_expressions)

    def _avoid_deadlocks(self, uct_sort, lambda_b_function, instance_count):
        '''
        Adds formula that ensures that for each global state s for which
        :math:`lambda^B(q, s)` is true, there exists at least one enabled local
        transition (i.e., there exists an input s.t. one transition is enabled)

        :param uct_sort: UCT state sort
        :param lambda_b_function: lambda^B function
        :param instance_count: cut-off for the currently encoded automaton
        '''
        uct_state = Const('q', uct_sort)
        global_state_var_tuples = \
            self.encoder_info.get_global_state_tuple_variables(instance_count,
                                                               suffix="curr")
        current_global_state_expr_dict = dict(global_state_var_tuples)

        composition_state = [uct_state] + [var for _, var
                                           in global_state_var_tuples]

        enabled_exprs = []
        for (k, i), local_state in global_state_var_tuples:
            templ_func = self.encoder_info.template_functions[k]
            input_vars = templ_func.get_fresh_input_variables()
            guard_set_expr = \
                self._build_state_guard_expression(
                    current_global_state_expr_dict, (k, i))
            if input_vars:
                # we do not need this constraint if there is no input variable
                enabled_exprs.append(
                     Exists(input_vars,
                            templ_func.is_any_enabled([local_state] +
                                                      input_vars +
                                                      [guard_set_expr])))

        # pylint: disable=star-args
        if enabled_exprs:
            constraint = ForAll(composition_state,
                                Implies(lambda_b_function(composition_state),
                                        Or(*enabled_exprs)))
            self.encoder_info.solver.add(constraint)

    def _encode_swen_automaton(self, instance_count):
        if len(instance_count) > 1:
            raise ValueError()

        uct_state = Datatype('Q_env')
        for idx in range(instance_count[0] + 1):
            uct_state.declare('q_env_' + str(idx))
        uct_state = uct_state.create()
        uct_states_dict = {uct_state.constructor(i).name():
                           getattr(uct_state, uct_state.constructor(i).name())
                           for i in range(instance_count[0] + 1)}

        current_global_state_expr_dict = \
            dict(self.encoder_info.get_global_state_tuple_variables(
                     instance_count, "curr"))

        next_global_state_expr_dict = \
            dict(self.encoder_info.get_global_state_tuple_variables(
                     instance_count, "next"))

        input_signals_dict = \
            dict(self.encoder_info.get_tuple_input_signals(instance_count))
        input_signal_expr_dict = \
            {sig: Bool(str(sig))
             for process_signals in input_signals_dict.values()
             for sig in process_signals}

        def _get_input_for_process(k, i):
            return [input_signal_expr_dict[sig]
                    for sig in input_signals_dict[(k, i)]]

        # declare lambda functions
        lambda_b_arg_sorts = \
            [uct_state] + \
            self.encoder_info.get_global_state_sorts(instance_count) + \
            [BoolSort()] * (len(input_signals_dict[(0, 0)]) + 1)
        lambda_b_function = Function('lambda_b_env', lambda_b_arg_sorts)

        lambda_s_arg_sorts = \
            [uct_state] + \
            self.encoder_info.get_global_state_sorts(instance_count) + \
            [BoolSort()] * (len(input_signals_dict[(0, 0)])) + [IntSort()]
        lambda_s_function = Function('lambda_s_env', lambda_s_arg_sorts)

        # initial constraint: lambda_B is true for all global states and inputs
        curr_system_state = self.encoder_info.get_sorted_global_state_exprs(
            current_global_state_expr_dict) + _get_input_for_process(0, 0)

        constr = ForAll(curr_system_state,
                        lambda_b_function(uct_states_dict['q_env_0'],
                                          *curr_system_state))
        self.encoder_info.solver.add(constr)


        # first scheduled process
        guard_set_expr = \
            self._build_state_guard_expression(
                current_global_state_expr_dict, (0, 0))
        first_any_enabled_expr = self.encoder_info.template_functions[0].is_any_enabled(
            [current_global_state_expr_dict[(0, 0)]] + \
            _get_input_for_process(0, 0) + \
            [guard_set_expr])
        curr_lambda_b = lambda_b_function(uct_states_dict['q_env_0'], *curr_system_state)
        next_lambda_b = lambda_b_function(uct_states_dict['q_env_1'], *curr_system_state)
        curr_lambda_s = lambda_s_function(uct_states_dict['q_env_0'], *curr_system_state)
        next_lambda_s = lambda_s_function(uct_states_dict['q_env_1'], *curr_system_state)

        constr = ForAll(curr_system_state,
                        Implies(And(curr_lambda_b, Not(first_any_enabled_expr)),
                                And(next_lambda_b, next_lambda_s > curr_lambda_s)))
        self.encoder_info.solver.add(constr)

        # all succeeding scheduled processes
        for step_idx in range(1, instance_count[0]):
            proc_idx = step_idx
            next_proc_idx = (step_idx + 1) % instance_count[0]
            if next_proc_idx == 0:
                next_proc_idx = 1
            q_curr = uct_states_dict['q_env_' + str(step_idx)]
            q_next = uct_states_dict['q_env_' + str(next_proc_idx)]

            transition_next_global_state_dict = \
                {k_i: (state_expr if k_i != (0, proc_idx)
                       else next_global_state_expr_dict[0, proc_idx])
                 for k_i, state_expr in current_global_state_expr_dict.items()}
            next_global_system_state = self.encoder_info.get_sorted_global_state_exprs(
                transition_next_global_state_dict) + _get_input_for_process(0, 0)

            curr_lambda_b = lambda_b_function(q_curr, *curr_system_state)
            curr_lambda_s = lambda_s_function(q_curr, *curr_system_state)

            guard_set_expr = \
                self._build_state_guard_expression(
                    current_global_state_expr_dict, (0, proc_idx))

            # first case
            any_enabled_expr = self.encoder_info.template_functions[0].is_any_enabled(
                [current_global_state_expr_dict[(0, proc_idx)]] + \
                _get_input_for_process(0, proc_idx) + \
                [guard_set_expr])
            next_lambda_b = lambda_b_function(q_next, *curr_system_state)
            next_lambda_s = lambda_s_function(q_next, *curr_system_state)
            constr = ForAll(
                curr_system_state + _get_input_for_process(0, proc_idx),
                Implies(And(curr_lambda_b, Not(first_any_enabled_expr), Not(any_enabled_expr)),
                        And(next_lambda_b, next_lambda_s >= curr_lambda_s)))
            self.encoder_info.solver.add(constr)

            # second case
            forall_expr = curr_system_state + [next_global_state_expr_dict[(0, proc_idx)]]
            delta_enabled_expr = self.encoder_info.template_functions[0].delta_enabled_function(
                [current_global_state_expr_dict[(0, proc_idx)]] + \
                _get_input_for_process(0, proc_idx) + \
                [next_global_state_expr_dict[(0, proc_idx)], guard_set_expr])
            next_lambda_b = lambda_b_function(q_next, *next_global_system_state)
            next_lambda_s = lambda_s_function(q_next, *next_global_system_state)

            constr = ForAll(
                forall_expr,
                Implies(And(curr_lambda_b, Not(first_any_enabled_expr), delta_enabled_expr),
                        And(next_lambda_b, next_lambda_s > curr_lambda_s)))
            self.encoder_info.solver.add(constr)

    def encode_automata(self, automata_infos):
        encoded_swen_automaton = False
        for automaton_index, automaton, instance_count, is_arch_property \
                in automata_infos:
            if not encoded_swen_automaton and is_arch_property and len(instance_count) == 1:
                self._encode_swen_automaton(instance_count)
                encoded_swen_automaton = True
            self._encode_automaton(automaton_index, automaton,
                                  instance_count, is_arch_property)

    def _encode_automaton(self, automaton_index, automaton, instance_count,
                          is_architecture_specific):
        '''
        Encodes the given automaton

        :param automaton_index: Automaton index used for identifying the lambda
                                function corresponding to the automaton
        :param automaton: Automaton instance
        :param instance_count: Cut-off associated with the current automaton
        :param is_architecture_specific: Whether automaton is required
                                         by the used architecture
        '''
        # declare UCT state
        uct_state = Datatype('Q_%d' % automaton_index)
        state_prefix = 'q%d_' % automaton_index

        nodes_list = list(automaton.nodes)
        for node in nodes_list:
            uct_state.declare(state_prefix + node.name)
        uct_state = uct_state.create()
        uct_states_dict = {nodes_list[i].name:
                           getattr(uct_state, uct_state.constructor(i).name())
                           for i in range(len(nodes_list))}

        # declare lambda functions
        lambda_b_arg_sorts = \
            [uct_state] + \
            self.encoder_info.get_global_state_sorts(instance_count) + \
            [BoolSort()]

        lambda_b_function = Function('lambda_b_%d' % (automaton_index),
                                     lambda_b_arg_sorts)

        lambda_s_arg_sorts = \
            [uct_state] + \
            self.encoder_info.get_global_state_sorts(instance_count) + \
            [IntSort()]

        lambda_s_function = Function('lambda_s_%d' % (automaton_index),
                                     lambda_s_arg_sorts)

        # avoid global deadlocks in case of the fairness property
        if is_architecture_specific:
            self._avoid_deadlocks(uct_state, lambda_b_function,
                                  instance_count)

        assert len(automaton.initial_sets_list) == 1
        initial_uct_states = [uct_states_dict[node.name]
                              for node in automaton.initial_sets_list[0]]
        initial_system_states = \
            self.encoder_info.get_initial_system_states(instance_count)

        # list of tuples in format (q0, (t1, t2, ...))
        initial_state_tuples = product(*[initial_uct_states,
                                         initial_system_states])
        # merge tuples
        initial_state_tuples = [tuple([item[0]] + list(item[1]))
                                for item in initial_state_tuples]

        logging.debug("Automaton %d   Initial states: %s",
                      automaton_index, initial_state_tuples)

        for initial_uct_state in initial_state_tuples:
            self.encoder_info.solver.add(lambda_b_function(initial_uct_state))
            if not self.use_scc:
                self.encoder_info.solver.add(
                    lambda_s_function(initial_uct_state) == 0)

        # assignment of the scheduling variables the particular processes
        # are scheduled (k,i) -> scheduling variable assignment list
        tuple_schedule_values = scheduler.get_schedule_values(instance_count)
        scheduling_signals = scheduler.get_scheduling_signals(instance_count)

        # used for SCC lambda_s optimization
        sccs = build_state_to_rejecting_scc(automaton)
        scc_lambda_functions = \
            {scc: Function(
                 'lambda_s_%d_%d' % (automaton_index, scc_index),
                 [uct_state] +
                 self.encoder_info.get_global_state_sorts(instance_count) +
                 [BitVecSort(len(scc))])
             for scc_index, scc in enumerate(sccs.values())}

        current_global_state_expr_dict = \
            dict(self.encoder_info.get_global_state_tuple_variables(
                     instance_count, "curr"))

        input_signals_dict = \
            dict(self.encoder_info.get_tuple_input_signals(instance_count))

        input_signal_expr_dict = \
            {sig: Bool(str(sig))
             for process_signals in input_signals_dict.values()
             for sig in process_signals}

        # dictionary of output signals -> function call
        output_signal_expr_dict = \
            self.encoder_info.get_output_signal_expr_dict(instance_count)

        placeholder_signals_set = \
             set(self.architecture.get_placeholder_signals(instance_count))

        # we make a list of transitions in order
        # to reduce the number of for loops
        transitions = [(src_node, transition, target_node_info)
                       for src_node in automaton.nodes
                       for transition, target_node_infos
                       in src_node.transitions.items()
                       for target_node_info in target_node_infos[0]]

        for src_node, transition, target_node_info in transitions:
            target_node, is_rejecting_target_node = target_node_info

            logging.debug("Automaton: %d: %s->%s, condition: %s",
                          automaton_index, src_node.name, target_node.name,
                          transition)

            for (k, i), scheduling_assignment in tuple_schedule_values:
                template_function = self.encoder_info.template_functions[k]
                # only consider constraint if scheduling assignment
                # matches the label
                if not self._compare_scheduling(scheduling_signals,
                                                scheduling_assignment,
                                                transition):
                    logging.debug("\tSKIP %s->%s, condition: %s, "
                                  "scheduling=%s", src_node.name,
                                  target_node.name, transition,
                                  str(scheduling_assignment))
                    continue

                current_local_state_expr = \
                    current_global_state_expr_dict[(k, i)]
                next_local_state_expr = \
                    self.encoder_info.get_global_state_variable(k, i,
                                                                suffix="next")

                next_global_state_expr_dict = \
                    {k_i: (state_expr if k_i != (k, i)
                           else next_local_state_expr)
                     for k_i, state_expr
                     in current_global_state_expr_dict.items()}

                # build guard expression
                guard_set_expr = \
                    self._build_state_guard_expression(
                        current_global_state_expr_dict, (k, i))

                transition_keys = set(transition.keys())
                used_input_signals = transition_keys.intersection(
                    set(input_signal_expr_dict.keys()))
                used_output_signals = transition_keys.intersection(
                    output_signal_expr_dict.keys())
                used_placeholder_signals = transition_keys.intersection(
                    placeholder_signals_set)
                used_scheduler_signals = transition_keys.intersection(
                    set(scheduling_signals))

                assert len(used_input_signals) + \
                       len(used_output_signals) + \
                       len(used_placeholder_signals) + \
                       len(used_scheduler_signals) == len(transition.items())

                condition = []

                for input_signal in used_input_signals:
                    condition.append(input_signal_expr_dict[input_signal] ==
                                     BoolVal(transition[input_signal]))

                for output_signal in used_output_signals:
                    sig_k_i = (output_signal.template_index,
                               output_signal.instance_index)
                    condition.append(output_signal_expr_dict[output_signal](
                        current_global_state_expr_dict[sig_k_i]) ==
                        BoolVal(transition[output_signal]))

                for ph_signal in used_placeholder_signals:
                    ph_proc = (ph_signal.template_index,
                               ph_signal.instance_index)

                    ph_guard_set_expr = \
                        self._build_state_guard_expression(
                           current_global_state_expr_dict, ph_proc)

                    ph_local_state_expr = \
                        current_global_state_expr_dict[ph_proc]
                    ph_local_input_exprs = \
                        [input_signal_expr_dict[sig]
                         for sig in input_signals_dict[ph_proc]]

                    ph_cond = self.encoder_info.get_placeholder_expr(
                        (k, i), ph_proc, ph_signal, transition[ph_signal],
                        ph_local_state_expr, ph_local_input_exprs,
                        ph_guard_set_expr)

                    condition.append(ph_cond)

                condition_expression = True
                if len(condition) > 0:
                    # pylint:disable=star-args
                    condition_expression = And(*condition)

                # build lambda_sharp expression
                current_run_graph_state_expr = \
                    [uct_states_dict[src_node.name]] + \
                    self.encoder_info.get_sorted_global_state_exprs(
                        current_global_state_expr_dict)

                next_run_graph_state_expr = \
                    [uct_states_dict[target_node.name]] + \
                    self.encoder_info.get_sorted_global_state_exprs(
                        next_global_state_expr_dict)

                lambda_s_req_expr = None
                if self.use_scc:
                    logging.debug("Use LAMBDA_SCC optimization")
                    lambda_s_req_expr = True
                    current_scc = sccs.get(src_node)

                    if current_scc is not None and \
                            current_scc == sccs.get(target_node):
                        scc_ls_func = scc_lambda_functions[current_scc]
                        lambda_s_req_expr = \
                            [UGE, UGT][is_rejecting_target_node](
                                scc_ls_func(next_run_graph_state_expr),
                                scc_ls_func(current_run_graph_state_expr))
                else:
                    # using no lambda_s optimizations
                    annotation_diff = [0, 1][is_rejecting_target_node]
                    lambda_s_req_expr = (
                        lambda_s_function(next_run_graph_state_expr) >=
                        lambda_s_function(current_run_graph_state_expr) +
                        IntVal(annotation_diff))

                # build final forall
                # the next_global_state_expr_dict also contains
                # the current states (except for the considered local
                # state
                forall_quantified_exprs = \
                    [current_local_state_expr] + \
                    list(next_global_state_expr_dict.values()) + \
                    list(input_signal_expr_dict.values())

                local_input_exprs = [input_signal_expr_dict[sig]
                                     for sig in input_signals_dict[(k, i)]]

                delta_enabled_parameters = \
                    [current_local_state_expr] + local_input_exprs + \
                    [next_local_state_expr, guard_set_expr]

                extended_condition_expr = \
                    template_function.delta_enabled_function(
                        delta_enabled_parameters)

                # implicit self-loop # TODO: currently disabled
                if False and is_architecture_specific:
                    extended_condition_expr = \
                        Or(extended_condition_expr,
                           And(current_local_state_expr ==
                               next_local_state_expr,
                               Not(template_function.is_any_enabled(
                                       [current_local_state_expr] + \
                                       local_input_exprs + \
                                       [guard_set_expr]))))

                expr = ForAll(forall_quantified_exprs, Implies(
                    And(lambda_b_function(current_run_graph_state_expr),
                        condition_expression, extended_condition_expr),
                    And(lambda_b_function(next_run_graph_state_expr),
                        lambda_s_req_expr)))

                logging.debug("\tADD  %s->%s, condition: %s, scheduling=%s",
                              src_node.name, target_node.name,
                              transition, str(scheduling_assignment))

                self.encoder_info.solver.add(expr)

    @property
    def use_scc(self):
        '''
        Return whether we use SCC optimization
        '''
        return self._encoding_optimization & EncodingOptimization.LAMBDA_SCC

    @abstractmethod
    def _init_guard_size(self):
        '''
        Initializes the :attr:`encoder_info.guard_size` variable
        '''
        pass

    @abstractmethod
    def _encode_template_functions(self):
        '''
        Creates required :class:`TemplateFunction` instances and
        stores them into :attr:`self.encoder_info.template_functions

        :note: This method is abstract because the concrete
               :class:`TemplateFunction`class depends on the particular
               encoding.
        '''
        pass
