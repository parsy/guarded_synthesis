'''
Created on 15.05.2014

@author: simon
'''
from smt.api.templatemodel import ApiTemplateModel


class StateGuardedTemplateModel(ApiTemplateModel):
    '''
    Stores label-guarded synthesis results for one particular template
    '''
    def __init__(self, model, templatefunction):
        super().__init__(model, templatefunction)

        # retrieve the bit assignments of the different states
        self.guard_bit_states_dict = \
            {self.model.evaluate(
                self._template_function.state_guard(state)).as_long():
             {str(state)} for state in self._internal_states}

    def _get_sorted_guard_bits_str(self):
        return "{%s}" % ", ".join(["%s: %s" % (val, state)
                                   for val, state in
                                   sorted(self.guard_bit_states_dict.items())])
