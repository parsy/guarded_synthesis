'''
Created on 15.03.2014

@author: simon
'''
from functools import reduce
import logging

from z3 import Function, BoolSort, BitVecSort, ForAll, BitVecs, BitVecVal, \
    Implies, And, Const, Exists, BitVec, Not, Sum, ZeroExt, Extract

from architecture.guarded_system import DisjunctiveGuardedArchitecture, \
    ConjunctiveGuardedArchitecture, GuardedArchitecture
from math import ceil, log


class ArchitectureEncoder(object):
    '''
    Contains functionality to add architecture specific formulae
    to the SMT context
    '''

    def __init__(self, encoder_info, architecture):
        '''
        Constructor
        '''
        self._encoder_info = encoder_info
        self._architecture = architecture

    def define_eval_guard(self):
        '''
        Defines the guard evaluation function for disjunctive and
        conjunctive guarded systems
        '''
        self._encoder_info.eval_guard = \
            Function('eval_guard',
                     BitVecSort(self._encoder_info.guard_size),
                     BitVecSort(self._encoder_info.guard_size),
                     BoolSort())

        state_set, guard = BitVecs('state_set guard',
                                   self._encoder_info.guard_size)

        if isinstance(self._architecture, DisjunctiveGuardedArchitecture):
            self._encoder_info.solver.add(
                ForAll([state_set, guard],
                       self._encoder_info.eval_guard(state_set, guard) ==
                       ((state_set & guard) !=
                        BitVecVal(0, self._encoder_info.guard_size))))

        elif isinstance(self._architecture, ConjunctiveGuardedArchitecture):
            self._encoder_info.solver.add(
                ForAll([state_set, guard],
                       self._encoder_info.eval_guard(state_set, guard) ==
                       And(guard != BitVecVal(0, self._encoder_info.guard_size),
                           ((state_set | guard) == guard))))

    def add_guard_constraints(self):
        '''
        Adds architecture specific guard constraints for each template

        * system must be non-input-blocking
        * determinism regarding (current state, inputs, guard set)
        * conjunctive guards must include all templates' initial states
        * conjunctive guards must be 1-guards.
        '''
        if isinstance(self._architecture, GuardedArchitecture):
            for templ_func in self._encoder_info.template_functions:
                if len(templ_func.get_fresh_input_variables()) == 0:
                    continue

                # non-input blocking (enabledness for all or for
                # none input values)
                guard_params = templ_func.get_fresh_guard_params_variables()
                curr_state = guard_params[0]
                lhs_input_vars = guard_params[1:-1]
                gs_param = BitVec('gs', templ_func._encoder_info.guard_size)

                lhs = Exists(lhs_input_vars,
                             templ_func.is_any_enabled([curr_state] +
                                                       lhs_input_vars +
                                                       [gs_param]))

                rhs_input_vars = templ_func.get_fresh_input_variables()

                rhs = ForAll(rhs_input_vars,
                             templ_func.is_any_enabled([curr_state] +
                                                       rhs_input_vars +
                                                       [gs_param]))

                forall_vars = [curr_state, gs_param]

                impl = ForAll(forall_vars,
                              Implies(lhs, rhs))
                # input independent enabledness aka non-inputblocking
                # self._encoder_info.solver.add(impl)

                # ensure determinism regarding (current state,inputs,guard set)
                guard_parameters = \
                    templ_func.get_fresh_guard_params_variables()[:-1]
                gs_param = BitVec('gs', self._encoder_info.guard_size)

                successor_state_1 = Const('t_next1', templ_func.state_sort)
                successor_state_2 = Const('t_next2', templ_func.state_sort)

                function_parameters_1 = \
                    guard_parameters + \
                    [successor_state_1, gs_param]

                function_parameters_2 = \
                    guard_parameters + \
                    [successor_state_2, gs_param]

                forall_parameters = \
                    guard_parameters + \
                    [successor_state_1,
                     successor_state_2,
                     gs_param]

                # there is only one enabled transition for a given
                # tuple (current state, inputs, guard set)
                constraint = ForAll(
                    forall_parameters,
                    Implies(
                        And(templ_func.is_enabled(function_parameters_1),
                            successor_state_1 != successor_state_2),
                        Not(templ_func.is_enabled(function_parameters_2))))

                self._encoder_info.solver.add(constraint)

        if isinstance(self._architecture, ConjunctiveGuardedArchitecture):
            initial_bv_list = \
                [templ_func.state_guard(initial_state)
                 for templ_func in self._encoder_info.template_functions
                 for initial_state in templ_func.get_initial_states()]
            initial_bv = reduce(lambda x, y: x | y, initial_bv_list)

            for templ_func in self._encoder_info.template_functions:
                guard_params = templ_func.get_fresh_guard_params_variables()

                # each non-empty guard set must contain the initial
                # states of all templates
                constraint = \
                    ForAll(
                        guard_params,
                        Implies(
                            (templ_func.guard_function(guard_params) !=
                             BitVecVal(0, self._encoder_info.guard_size)),
                            (templ_func.guard_function(guard_params) &
                             initial_bv == initial_bv)))
                self._encoder_info.solver.add(constraint)

                # each guard must be either an trivial or a 1-guard
                guard_bit_sum_size = int(ceil(log(self._encoder_info.guard_size, 2))) + 1
                constraint = \
                    ForAll(
                        guard_params, Implies(
                            (templ_func.guard_function(guard_params) !=
                             BitVecVal(0, self._encoder_info.guard_size)),
                            Sum([ZeroExt(guard_bit_sum_size, Extract(i, i, templ_func.guard_function(guard_params)))
                                 for i in range(self._encoder_info.guard_size)]) >= self._encoder_info.guard_size - 1))
                self._encoder_info.solver.add(constraint)


        logging.debug("Solver satisfiability after adding "
                      "architecture constraints: %s",
                      str(self._encoder_info.solver.check()))
