'''
Created on 30.06.2014

@author: simon
'''

from itertools import product
from z3 import Const, BoolVal

TEMPLATE_STATE_VARIABLE_PATTERN = "t_{k}_{i}{suffix}"


class PyZ3EncoderInfo:
    '''
    Encapsulates data that is used by synthesis encoder and template encoders
    '''
    def __init__(self):
        self.solver = None
        self.is_scheduled = None
        self.eval_guard = None
        self.architecture_encoder = None
        self.template_functions = None
        self.spec = None

        self.guard_size = None
        self.guard_slice_sizes = None

    def get_global_state_tuple_sorts(self, instance_count):
        return [((k, i), self.template_functions[k].state_sort)
                for k, num_instances in enumerate(instance_count)
                for i in range(num_instances)]

    def get_global_state_sorts(self, instance_count):
        return [sort for _, sort in
                self.get_global_state_tuple_sorts(instance_count)]

    def get_global_state_tuple_variables(self, instance_count, suffix):
        if suffix:
            suffix = "_" + suffix

        tuple_sort_list = self.get_global_state_tuple_sorts(instance_count)
        return [((k, i), Const(TEMPLATE_STATE_VARIABLE_PATTERN.format(k=k, i=i,
                                                             suffix=suffix),
                               sort)) for (k, i), sort in tuple_sort_list]

    def get_global_state_variables(self, instance_count, suffix=""):
        return [var for _, var in
                self.get_global_state_tuple_variables(instance_count, suffix)]

    def get_tuple_input_signals(self, instance_count):
        return [((k, i), self.template_functions[k].get_input_signals(i))
                for k, num_instances in enumerate(instance_count)
                for i in range(num_instances)]

    def get_output_signal_expr_dict(self, instance_count):
        output_functions_dict = \
            {output_signal: output_function
             for k, num_instances in enumerate(instance_count)
             for i in range(num_instances)
             for output_signal, output_function in
             self.template_functions[k].
             get_output_signals_function_dict(i).items()}

        return output_functions_dict

    def get_global_state_variable(self, k, i, suffix):
        if suffix:
            suffix = "_" + suffix
        return Const(
            TEMPLATE_STATE_VARIABLE_PATTERN.format(k=k, i=i, suffix=suffix),
            self.template_functions[k].state_sort)

    def get_placeholder_expr(self, scheduled_proc, ph_proc, ph_signal,
                             ph_value, ph_local_state, ph_local_inputs,
                             ph_guard_set_expr):

        k, _ = ph_proc
        ph_template_func = self.template_functions[k]

        if ph_signal.name.startswith('active'):
            return (scheduled_proc == ph_proc) == BoolVal(ph_value)
        elif ph_signal.name.startswith('enabled'):
            return ph_template_func.is_any_enabled(
                [ph_local_state] + ph_local_inputs +
                [ph_guard_set_expr]) == ph_value
        elif ph_signal.name.startswith('init'):
            ph_initial_states = ph_template_func.get_initial_states()
            assert len(ph_initial_states) == 1
            return (ph_local_state == ph_initial_states[0]) == ph_value

        raise Exception("Unknown placeholder signal: %s" %
                        ph_signal.name)

    def get_sorted_global_state_exprs(self, global_state_expr_dict):
        return [state for _, state in
                sorted(global_state_expr_dict.items(),
                       key=lambda item: item[0])]

    def get_initial_system_states(self, instance_count):
        """
        Return all possible combinations of local initial states
        """
        initial_state_sets_list = \
            [self.template_functions[k].get_initial_states()
             for k, num_instances in enumerate(instance_count)
             for _ in range(num_instances)]
        return product(*initial_state_sets_list)  # pylint: disable=star-args
