'''
Created on 23.07.2014

@author: simon
'''
import logging

from z3 import sat

from smt.encoder_base import EncodingOptimization
from smt.api.encoder import PyZ3Encoder


class LabelStateEncoder(PyZ3Encoder):
    def __init__(self, spec, architecture,
        encoding_optimization=EncodingOptimization.NONE):
        PyZ3Encoder.__init__(self, spec, architecture,
                             encoding_optimization=encoding_optimization)
        self.guard_slice_sizes = None
        self._logger = logging.getLogger(__name__)

    def _init_guard_size(self):
        self.encoder_info.guard_slice_sizes = self.guard_slice_sizes
        self.encoder_info.guard_size = sum(self.encoder_info.guard_slice_sizes)

    def _encode_template_functions(self):
        # create templates
        from smt.api.labelguarded.templatefunction import TemplateFunction \
            as LabelGuardedTemplateFunction
        from smt.api.stateguarded.templatefunction import TemplateFunction \
            as StateGuardedTemplateFunction

        self.encoder_info.template_functions = \
            [[LabelGuardedTemplateFunction, StateGuardedTemplateFunction]
             [self.guard_slice_sizes[i] == self.spec.bound[i]]
             (template, self, self.encoder_info, self.spec)
             for i, template in enumerate(self.spec.templates)]

        self._logger.debug(self.encoder_info.template_functions)

    def check(self):
        s = self.encoder_info.solver
        res = s.check()
        model = s.model()
        self._logger.debug("Solving result: %s", res)

        template_models = None
        if res == sat:
            template_models = {}
            guard_bits_dict = {}
            for template_function in self.encoder_info.template_functions:
                template_index = template_function.template_index
                template_model = template_function.get_model(model)
                template_models[template_index] = template_model

                template_model.update_guard_bit_dict(guard_bits_dict)
            for template_model in template_models.values():
                template_model.init_transition_guards(guard_bits_dict)

        return (res == sat, template_models)
