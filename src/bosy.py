'''
Main program logic of parameterized bounded synthesis for guarded systems
'''
import logging

from datastructures import specification
from datastructures.specification import ArchitectureGuarantee
from helpers import automata_helper
from helpers.ast_visitor import ASTInstantiateFormulaVisitor
from interfaces.parser_expr import and_expressions, BinOp
from translation2uct.ltl2automaton import Ltl2UCW
from smt.encoder import SMTEncoderFactory
import config

LOG = logging.getLogger("bosy")


class BoundedSynthesis:

    def __init__(self, spec_filename, architecture):
        """
        :param spec_filename: path of LTL specification
        :param architecture: class which represents the desired
                             system architecture
        """
        self.min_bound = None
        self.max_increments = None
        self.encoder_type = None
        self.instance_count = None
        self.encoder_optimization = None
        self.test_mode = False

        self.spec_filename = spec_filename

        # load specification
        self.spec = specification.Specification(filename=spec_filename)
        # initialize architecture
        self.arch = architecture(self.spec)

        self.ltl2ucw = Ltl2UCW(config.LTL3BA_PATH)

    def solve(self):
        '''
        Bounded synthesis

        Passes the following steps iteratively until the solver returns
        SAT in the solving step.

        * Set bound
        * Determine cut-off
        * Instantiate properties and create UCT automata
        * Encode formula in SMT
        * Solve

        :return: model or None if no model was found
        '''
        self.spec.bound = self.min_bound

        status = None
        model = None

        for round_index in range(self.max_increments + 1):
            # increase bound for all templates
            self.spec.bound = tuple([b + round_index for b in self.min_bound])
            LOG.info("Set bound to %s", str(self.spec.bound))

            # recalculate cut-off
            deadlock_detection_cutoff, guarantee_cutoffs_list = \
                self.arch.determine_cutoffs(self.spec.bound,
                                            self.instance_count)

            if self.test_mode:
                # in test mode, we set the cut-off to the instance count
                deadlock_detection_cutoff = self.instance_count
                guarantee_cutoffs_list = [(guarantee, self.instance_count)
                                          for guarantee, _ in
                                          guarantee_cutoffs_list]

            LOG.info("Deadlock Detection Cut-Off: %s",
                     str(deadlock_detection_cutoff))

            # add architecture guarantees with max. cut-off
            # (architecture guarantees must hold for all
            # instances in the system)
            arch_guarantees = self.arch.get_architecture_guarantees(
                range(0, self.spec.templates_count))
            arch_guarantee_cutoffs_list = [(guarantee,
                                            deadlock_detection_cutoff)
                                           for guarantee in arch_guarantees]
            guarantee_cutoffs_list = (arch_guarantee_cutoffs_list +
                                      guarantee_cutoffs_list)

            LOG.debug("-------------------------------------------")
            LOG.debug("Guarantees")
            for guarantee, guarantee_cutoff in guarantee_cutoffs_list:
                LOG.debug("%s --> cut-off: %s)",
                          guarantee, guarantee_cutoff)
            LOG.debug("-------------------------------------------\n")

            # build assumptions set
            if len(self.spec.assumptions) > 0:
                raise Exception("Specification assumptions are "
                                "currently not supported!")

            arch_assumptions = self.arch.get_architecture_assumptions(
                range(0, self.spec.templates_count))
            assumptions = self.spec.assumptions + arch_assumptions
            LOG.debug("-------------------------------------------")
            LOG.debug("Assumptions")
            for idx, assumption in enumerate(assumptions):
                LOG.debug("(%s) %s)",
                          ["spec", "arch"][idx >= len(assumptions) -
                                           len(arch_assumptions)], assumption)
            LOG.debug("-------------------------------------------\n")

            # create properties
            # properties are either tuples (assumption, guarantee) of
            # quadruples (assumption, guarantee, cutoff)
            # ignore_cutoff is set if the guarantee-specific cut-off is
            # larger than the number of specified instances
            properties = [(assumptions, guarantee, guarantee_cutoff)
                          for (guarantee, guarantee_cutoff)
                          in guarantee_cutoffs_list]

            # add architecture properties
            arch_properties = [(assumption, guarantee,
                                deadlock_detection_cutoff)
                               for assumption, guarantee in
                               self.arch.get_architecture_properties(
                                   range(0, self.spec.templates_count))]

            properties = arch_properties + properties

            LOG.info("-------------------------------------------")
            LOG.info("Properties")
            for assumptions, guarantee, \
                    guarantee_cutoff in properties:
                LOG.info("(%s, %s) --> cut-off: %s)",
                         assumptions, guarantee,
                         guarantee_cutoff)
            LOG.info("-------------------------------------------")

            # instantiate properties
            instantiated_properties = \
                self.instantiate_properties(properties)

            # get automaton for each property
            property_automata = [self.ltl2ucw.convert(prop)
                                 for prop in instantiated_properties]

            for i, prop in enumerate(instantiated_properties):
                LOG.info(prop)
                LOG.info("\t states: %s", len(property_automata[i].nodes))

            # encode automata
            encoder = SMTEncoderFactory().create(self.encoder_type)(
                self.spec, self.arch, self.encoder_optimization)
            encoder.encode()

            encoder.encode_automata([(i, property_automaton, properties[i][-1],
                                      i < len(arch_properties))
                                     for i, property_automaton in
                                     enumerate(property_automata)])
            status, model = encoder.check()

            # for constraint in encoder.encoder_info.solver.assertions():
            #    LOG.debug(constraint)

#             m = encoder.encoder_info.solver.model()
#             for m in model:
#                 print(m)

            LOG.info("Status: %s", status)
            LOG.info("Model: %s", model)

            # extract solution
            if status:
                return model

    def _truncate_cutoff(self, cutoff):
        return tuple([min(self.instance_count[i], cutoff[i])
                      for i in range(len(self.instance_count))])

    def instantiate_properties(self, properties):
        '''
        Instantiates the quantified property formulas

        :param properties: list of tuples (assumption, guarantee) or
               (assumption, guarantee, guarantee_cutoff)
        '''
        return [self.instantiate_property(prop) for prop in properties]

    def instantiate_property(self, prop):
        '''
        Instantiates the given property

        :param prop: See ``properties`` in :func:`instantiate_properties`
        '''
        # extract assumptions and guarantees
        assumptions, guarantee, guarantee_cutoff = prop

        LOG.debug(prop)

        ast_visitor = ASTInstantiateFormulaVisitor(guarantee_cutoff)

        guarantee_instances_dict = assumption_instances_dict = \
            {i: range(guarantee_cutoff[i])
             for i in range(len(guarantee_cutoff))}

        if not isinstance(guarantee, ArchitectureGuarantee):
            # use symmetry for non-architecture guarantees
            guarantee_instances_dict = \
                self.arch.determine_guarantee_instances_dict(guarantee)

        instantiated_guarantees = and_expressions(
            [conjunct for conjunct in
             ast_visitor.visit(guarantee, guarantee_instances_dict)])
        LOG.debug("Guarantees instantiated: %s", instantiated_guarantees)

        instantiated_assumptions = None
        if assumptions and self.is_liveness_property(instantiated_guarantees):
            # instantiate assumptions
            instantiated_assumptions = \
                and_expressions([conjunct for formula in assumptions
                                 for conjunct in
                                 ast_visitor.visit(formula,
                                                   assumption_instances_dict)])

        LOG.debug("Assumptions instantiated: %s", instantiated_assumptions)

        # return implication or just guarantees
        if instantiated_assumptions is not None:
            return BinOp('->', instantiated_assumptions,
                         instantiated_guarantees)
        else:
            return instantiated_guarantees

    def is_liveness_property(self, guarantee):
        '''
        Checks whether the given guarantee is a liveness guarantee
        :param guarantee:
        '''
        automaton = self.ltl2ucw.convert(guarantee)
        return not automata_helper.is_safety_automaton(automaton)


def print_spec_formulas(*spec_formulas):
    '''
    Prints some information for the given specification formulas
    '''
    for spec_formula in spec_formulas:
        print("Formula:", spec_formula.formula)
        print("  Indices:   ", str(spec_formula.indices)[1:-2])
        print("  Templates: ", str(spec_formula.template_indices)[1:-1])
        print("  Template-I:",
              list(spec_formula.template_instance_index_dict.items())[1:-1])
        print("")
