'''
Created on 30.06.2014

@author: simon
'''
from math import log, ceil

from interfaces.parser_expr import Signal


def get_scheduling_size(instance_count):
    '''
    Return the number of required scheduling variables for
    the given system size
    :param instance_count: System size
    '''
    return max(1, ceil(log(sum(instance_count), 2)))


def get_scheduling_signals(instance_count):
    '''
    Returns a list of signals that represent the Boolean scheduling
    variables

    The number of scheduling signals is determined by
    :func:`get_scheduling_size`
    '''
    return [Signal('sched_%d' % i)
            for i in reversed(range(0, get_scheduling_size(instance_count)))]


def get_schedule_values(instance_count):
    """
    Returns tuple ((k,i), sched_value), where sched_value is
    the boolean assignment (low endian)
    """

    # list of (template_index, instance_index) tuples based on
    # the currently stored cut-off
    template_instance_tuple_list = \
        [(template_index, inst_index)
         for template_index in range(0, len(instance_count))
         for inst_index in range(0, instance_count[template_index])]

    scheduling_size = get_scheduling_size(instance_count)

    def _get_bool_assignment(index_tuple_position):
        '''
        Calculates the assignment of the boolean scheduling variables s.t.
        the particular process identified by (template_index, instance_index)
        is scheduled.

        :param index_tuple_position: Index of the tuple in the (k,i) tuple list
        '''
        # binary representation of the tuple index
        binval = bin(index_tuple_position)[2:]
        bool_assignment = [binval[i] == '1' for i in range(len(binval))]

        # prepend False
        bool_assignment = [False] * (scheduling_size -
                                     len(bool_assignment)) + bool_assignment
        assert len(bool_assignment) == scheduling_size
        return bool_assignment

    return [(template_instance_tuple_list[i],
             _get_bool_assignment(i))
            for i in range(len(template_instance_tuple_list))]
